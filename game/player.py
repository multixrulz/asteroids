import math
import pyglet
from pyglet.window import key
from . import physical_object, resources, bullet, load


class Player(physical_object.PhysicalObject):

    def __init__(self, *args, lives=[], x, y, **kwargs):
        super().__init__(img=resources.player_image, x=x, y=y, *args, **kwargs)
        self.thrust = 300.0
        self.rotate_speed = 200.0
        self.key_handler = key.KeyStateHandler()
        self.keys = dict(left=False, right=False, up=False)
        self.engine_sprite = pyglet.sprite.Sprite(
            img=resources.engine_image, *args, **kwargs)
        self.engine_sprite.visible = False
        self.bullet_speed = 700.0
        self.reacts_to_bullets = False
        self._lives = lives
        self._startpos_x = x
        self._startpos_y = y
        self.rotation = -90

    def update(self, dt):
        if self.visible or self.passive:
            super(Player, self).update(dt)

            if self.key_handler[key.LEFT]:
                self.rotation -= self.rotate_speed * dt
            if self.key_handler[key.RIGHT]:
                self.rotation += self.rotate_speed * dt
            if self.key_handler[key.UP]:
                angle_radians = -math.radians(self.rotation)
                force_x = math.cos(angle_radians) * self.thrust * dt
                force_y = math.sin(angle_radians) * self.thrust * dt
                self.velocity_x += force_x
                self.velocity_y += force_y
                self.engine_sprite.rotation = self.rotation
                self.engine_sprite.x = self.x
                self.engine_sprite.y = self.y
                self.engine_sprite.visible = self.visible
            else:
                self.engine_sprite.visible = False

    def delete(self):
        self.engine_sprite.delete()
        super(Player, self).delete()

    def on_key_press(self, symbol, modifiers):
        if self.visible and not self.passive:
            if symbol == key.SPACE:
                self.fire()

    def fire(self):
        angle_radians = -math.radians(self.rotation)
        ship_radius = self.image.width/2
        bullet_x = self.x + math.cos(angle_radians) * ship_radius
        bullet_y = self.y + math.sin(angle_radians) * ship_radius
        new_bullet = bullet.Bullet(bullet_x, bullet_y, batch=self.batch)
        bullet_vx = (
            self.velocity_x +
            math.cos(angle_radians) * self.bullet_speed
        )
        bullet_vy = (
            self.velocity_y +
            math.sin(angle_radians) * self.bullet_speed
        )
        new_bullet.velocity_x = bullet_vx
        new_bullet.velocity_y = bullet_vy
        self.new_objects.append(new_bullet)

    def handle_collision_with(self, other_object):
        if other_object.is_bullet:
            self.dead = False
        else:
            self.dead = False
            self.visible = False
            self.engine_sprite.visible = False
            if len(self._lives) > 0:
                self._lives.pop()
                pyglet.clock.schedule_once(self.respawn, 1)
            else:
                self.dead = True

    def respawn(self, dt):
        self.passive = True
        self.visible = True
        self.x = self._startpos_x
        self.y = self._startpos_y
        self.velocity_x, self.velocity_y = 0.0, 0.0
        self.rotation = -90
        pyglet.clock.schedule_interval(self.ghost, 0.2)
        pyglet.clock.schedule_once(self.physical, 2)

    def ghost(self, dt):
        self.visible = not self.visible

    def physical(self, dt):
        pyglet.clock.unschedule(self.ghost)
        self.visible = True
        self.passive = False

    def add_life(self):
        self._lives.append(load.new_life(len(self._lives), self.batch))
