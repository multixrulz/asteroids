import math
import pyglet
import random
from pyglet.window import key
from . import physical_object, resources, laser, load


class Ufo(physical_object.PhysicalObject):

    def __init__(self, *args, **kwargs):
        self.player = kwargs.pop("player")
        super().__init__(img=resources.ufo_image, *args, **kwargs)
        self.laser_speed = 1000.0
        self.reacts_to_bullets = True
        self.reacts_to_lasers = False
        self.scale = 0.25
        self.x = - self.width / 2
        y_initial = 100 + 400 * random.random()
        y_final = 100 + 400 * random.random()
        ### Calculate velocities
        # Time to cross screen
        time_to_cross = 10.0
        self.velocity_x = 800 / time_to_cross
        # Time to go from y_initial to y_final is the same
        # as the time to cross the screen.
        self.velocity_y = (y_final - y_initial) / time_to_cross
        self.velocity_y_base = self.velocity_y
        self.y = y_initial
        pyglet.clock.schedule_once(self.wavy_y, 0.5)
        pyglet.clock.schedule_once(self.fire, 2)

    def wavy_y(self, dt):
        self.velocity_y = self.velocity_y_base + 100 - 200 * random.random()
        pyglet.clock.schedule_once(self.wavy_y, 0.5)

    def update(self, dt):
        self.x += self.velocity_x * dt
        self.y += self.velocity_y * dt
        if self.x >= 800 + self.width / 2:
            self.dead = True
            pyglet.clock.unschedule(self.wavy_y)
            pyglet.clock.unschedule(self.fire)
        self.check_bounds()

    def handle_collision_with(self, other_object):
        super().handle_collision_with(other_object)
        if self.dead == True:
            pyglet.clock.unschedule(self.wavy_y)
            pyglet.clock.unschedule(self.fire)

    def fire(self, dt):
        #angle_radians = -math.radians(self.rotation)
        angle_radians = -math.atan((self.player.y - self.y)/(self.player.x - self.x))
        laser_x = self.x + math.cos(angle_radians)
        laser_y = self.y - math.sin(angle_radians)
        new_laser = laser.Laser(laser_x, laser_y, batch=self.batch)
        laser_vx = (
            math.cos(angle_radians) * self.laser_speed
        )
        laser_vy = (
            - math.sin(angle_radians) * self.laser_speed
        )
        new_laser.velocity_x = laser_vx
        new_laser.velocity_y = laser_vy
        new_laser.rotation = math.degrees(angle_radians)
        self.new_objects.append(new_laser)
        pyglet.clock.schedule_once(self.fire, 2)
