import pyglet
from . import util

class PhysicalObject(pyglet.sprite.Sprite):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.velocity_x, self.velocity_y = 0.0, 0.0
        self.dead = False
        self.new_objects = []
        self.reacts_to_bullets = True
        self.is_bullet = False
        self.reacts_to_lasers = True
        self.is_laser = False
        self.passive = False
        self.points = 0

    def update(self, dt):
        self.x += self.velocity_x * dt
        self.y += self.velocity_y * dt
        self.check_bounds()

    def check_bounds(self):
        min_x = -self.width / 2
        min_y = -self.height / 2
        max_x = 800 + self.width / 2
        max_y = 600 + self.height / 2
        if self.x < min_x:
            self.x = max_x
        elif self.x > max_x:
            self.x = min_x
        if self.y < min_y:
            self.y = max_y
        elif self.y > max_y:
            self.y = min_y

    def collides_with(self, other_object):
        if ((self.passive or self.visible == False) or
            (other_object.passive or other_object.visible == False)):
            return False
        else:
            collision_distance = self.width/2 + other_object.width/2
            actual_distance = util.distance(self.position, other_object.position)
            return (actual_distance <= collision_distance)

    def handle_collision_with(self, other_object):
        if other_object.__class__ == self.__class__:
            self.dead = False
        else:
            if not self.reacts_to_bullets and other_object.is_bullet:
                self.dead = False
            elif self.is_bullet and not other_object.reacts_to_bullets:
                self.dead = False
            elif not self.reacts_to_lasers and other_object.is_laser:
                self.dead = False
            elif self.is_laser and not other_object.reacts_to_lasers:
                self.dead = False
            else:
                self.dead = True
