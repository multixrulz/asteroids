import pyglet
from . import physical_object, resources

class Laser(physical_object.PhysicalObject):
    """lasers fired by the ufo"""
    def __init__(self, *args, **kwargs):
        super(Laser, self).__init__(resources.laser_image, *args, **kwargs)
        pyglet.clock.schedule_once(self.die, 0.5)
        self.reacts_to_bullets = False
        self.is_laser = True
        self.scale = 0.4

    def die(self, dt):
        self.dead = True
