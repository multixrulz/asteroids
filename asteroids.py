#!/usr/bin/python

import pyglet
from pyglet.window import key
import random
from game import load, resources, physical_object, player, asteroid, ufo

class Asteroids():
    def __init__(self):
        self.game_over = False

        # Set up a window
        self.game_window = pyglet.window.Window(800, 600)
        @self.game_window.event
        def on_draw():
            self.game_window.clear()
            self.main_batch.draw()

        @self.game_window.event
        def on_key_press(symbol, modifiers):
            if self.game_over:
                self.reset_game()

        # Set up a batch to bulk-draw sprites
        self.main_batch = pyglet.graphics.Batch()

        self.gameover_label = pyglet.text.Label(
            font_size=50, x=400, y=300,
            anchor_x='center', anchor_y='center',
            batch=self.main_batch)
        self.restart_label = pyglet.text.Label(
            font_size=20, x=400, y=150,
            anchor_x='center', anchor_y='center',
            batch=self.main_batch)

        # Set up the score
        self.score_label = pyglet.text.Label(text="Score: 0",
            x=10, y=10, batch=self.main_batch)

       # Level
        self.level_label = pyglet.text.Label(text="level: 1",
            x=10, y=570, batch=self.main_batch)

        self.reset_game()

        # Tell pyglet to do its thing
        pyglet.clock.schedule_interval(self.update, 1/120.0)

    def reset_game(self):
        self.game_over = False
        self.gameover_label.text = ""
        self.restart_label.text = ""

        # Set up the lives icons
        lives = load.player_lives(3, batch=self.main_batch)

        # Initialize the player sprite
        self.player_ship = player.Player(x=400, y=300,
            batch=self.main_batch, lives=lives)
        self.game_window.push_handlers(self.player_ship.key_handler)
        self.game_window.push_handlers(self.player_ship)

        self.level = 1
        self.new_level = False

        self.score = 0

        self.game_objects = [self.player_ship]

        # Make the asteroids so we have something to shoot at
        self.create_asteroids()

        pyglet.clock.unschedule(self.new_ufo)
        pyglet.clock.schedule_once(self.new_ufo, 80 + 30 * random.random())

    def run(self):
        pyglet.app.run()

    def update(self, dt):
        for i in range(len(self.game_objects)):
            for j in range(i+1, len(self.game_objects)):
                obj_1 = self.game_objects[i]
                obj_2 = self.game_objects[j]
                if not obj_1.dead and not obj_2.dead:
                    if obj_1.collides_with(obj_2):
                        obj_1.handle_collision_with(obj_2)
                        obj_2.handle_collision_with(obj_1)

        to_add = []
        for obj in self.game_objects:
            obj.update(dt)
            to_add.extend(obj.new_objects)
            obj.new_objects = []

        new_points = 0
        for to_remove in [obj for obj in self.game_objects if obj.dead]:
            new_points += int(to_remove.points)
            if type(to_remove) == player.Player:
                # Game over
                self.game_over = True
                self.gameover_label.text = "Game Over"
                self.restart_label.text = "(Press any key to restart)"

            to_remove.delete()
            self.game_objects.remove(to_remove)

        self.game_objects.extend(to_add)

        if int(self.score / 10000) != int((self.score + new_points) / 10000):
            # Passed 10,000 points, get a new ship
            self.player_ship.add_life()
        self.score += new_points

        self.score_label.text = "Score: {}".format(self.score)

        self.level_label.text = "level: {}".format(self.level)

        if not self.new_level:
            if len([o for o in self.game_objects
                if type(o) == asteroid.Asteroid]) == 0:
                self.new_level = True
                self.level += 1
                if self.level >= 1:
                    ufo_delay = 4 + 2 * random.random()
                    pyglet.clock.schedule_once(self.new_ufo, ufo_delay)
                pyglet.clock.schedule_once(self.create_asteroids, 1)

    def create_asteroids(self, dt=0):
        # Make some asteroids so we have something to shoot at
        if self.level < 3:
            asteroid_count = 3
        elif self.level < 6:
            asteroid_count = 4
        elif self.level < 9:
            asteroid_count = 5
        elif self.level < 12:
            asteroid_count = 6
        elif self.level < 15:
            asteroid_count = 7
        else:
            asteroid_count = 8

        asteroids = load.asteroids(asteroid_count, self.player_ship.position,
            batch=self.main_batch)
        self.game_objects.extend(asteroids)
        self.new_level = False

    def new_ufo(self, dt=0):
        new_ufo = ufo.Ufo(batch=self.main_batch, player=self.player_ship)
        self.game_objects.append(new_ufo)
        pyglet.clock.schedule_once(self.new_ufo, 80 + 30 * random.random())

if __name__ == "__main__":
    game = Asteroids()
    game.run()
